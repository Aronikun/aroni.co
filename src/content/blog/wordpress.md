---
title: WordPress in 2025
description: The WordPress debacle and the risks of using it.
category: Blog
pubDate: 2025-01-03
blueskyUri: 3lfsp7cimn22i
---

I haven't found anything out there that offers a free content management system that can have a site live, on the net, in perfect working order and in minutes in the way that WordPress does. It's friendly for the user but not for the developer. Clients tend to exaggerate their expectations when working with WordPress. It's a blogging tool with a content management system attached. Using it for anything more will cost you strife, mental anguish, and money. Just [look under the hood](https://github.com/WordPress/WordPress). It's a hot mess.

In case you're wondering, the website you are reading right now is built in [**AstroJS**](http://astro.build). I have used lots of frameworks but I've come to like Astro. I'm a web dev, so I don't need WordPress and after recent events, you shouldn't either. I know this tool puts plenty of food on many peoples tables. This is why you should stay informed to keep that food coming.

## You don't need WordPress

I have used WordPress a lot and I liked it. If you used it for what it was intended for and no more, then it was a great tool that is free and easy to install / manage and use. You could have a website up, hosted and running in less than an hour. Dreamhost has automatic updates keeping it nice and secure without the need to install plugins. And without the plugins or paid templates, it's free. It's also [open source](https://en.wikipedia.org/wiki/Open_source).

I do understand it is a cesspit of nasty code, it's full of security holes and clients always demand the abundant use of sketchy plugins. And also the fact that on the frontend, there was never any resemblence of any design principles what-so-ever, I still used it. Often, there were situations where the benefits outweighed the doubts. Even now, in early 2025, a little less than half of all websites on the web are made with this tool.

But I will never use WordPress again. Not in any professional sense. And if you are currently using WordPress, I recommend you find a developer to remake your website using something else. The reason is simple; but how it got here is from a [series of events](https://notes.ghed.in/posts/2024/matt-mullenweg-wp-engine-debacle/) that in my mind, has turned [Matt Mullenweg](https://en.wikipedia.org/wiki/Matt_Mullenweg) and anyone that works for him into a villian. Making a website for a client using WordPress was [always a risk](https://sucuri.net/reports/2023-hacked-website-report/) but now its like working with a time bomb.

Let me try to fill you in.

- [WordPress](https://wordpress.org/) is a free and open source content management tool.
- Matt is the co-founder of WordPress. [He says he owns it](https://www.inc.com/dfreedman/matt-mullenweg-wordpress-wp-engine/90994969#:~:text=Mullenweg%20has%20made%20it%20clear,about%20his%20control%20over%20WordPress.). He also owns [Automattic](https://automattic.com/).
- [WP Engine](https://wpengine.com/) is a successful hosting company that uses WordPress, not owned by Matt.
- Matt singles out WP Engine as enemy number one at a WordCamp, which is a WordPress conference. You can watch it [here](https://www.youtube.com/watch?v=fnI-QcVSwMU) or you can [read an article from an attendee](https://www.briancoords.com/the-wcus-closing-i-wish-wed-had/). Brian sums it up much nicer.
- [Matt thinks WP Engine and their ways are evil](https://www.reddit.com/r/Wordpress/comments/1fn3mjr/comment/lokzvec/). *Read some of the comments*.
- WP Engine make Matt's antics public and hand a [cease and desist letter](https://wpengine.com/wp-content/uploads/2024/09/Cease-and-Desist-Letter-to-Automattic-and-Request-to-Preserve-Documents-Sent.pdf).
- Matt does [exactly the opposite](https://www.reddit.com/r/Wordpress/comments/1fnz0h6/comment/lon9wsg/) and instead, demands money.
- Matt [publicly berates WP Engine](https://wordpress.org/news/2024/09/wp-engine/) and leaves it posted.
- Interview with [Matt Mullenweg](https://www.youtube.com/watch?v=H6F0PgMcKWM).

He offered severence pay to anyone who disagreed with him. [Then 159 people took the offer](https://ma.tt/2024/10/alignment/). He frames it like his angels that decided to stay gave up a $126M severence just be with him and the holy WordPress. No. They gave up $30,000 or six months of salary, whichever is higher which he wrote **in bold!**

This tells me that the good guys are the ones who left. It is nice to get a severence but that offer is not something you'd take out of greed. Greed is working for and taking pay checks from someone who rallies public, unsolicited to side with his personal revenge.

## Good software comes from good people

It is my opinion that he is just not a good guy. He may not be aware of it, but he hypocritically does what he accuses WP Engine of doing:

> they break the trust and sanctity of our software’s promise to users to save themselves money so they can extract more profits from you

And at the start of his [WordPress News](https://wordpress.org/news/2024/09/wp-engine/).

> My own mother was confused

He runs, owns and operates [WordPress.com](https://wordpress.com/) under Automattic, a paid service - to be absolutely confused with [WordPress.org](https://wordpress.org/) the free open source software also "owned" by Matt.

Automattic turned over around $700M last year. I don't think he's short on cash. None of my business though. He can do with it what he wants. What I don't like is the manipulation.

Either way, it's sad what it has come to and its now changed from something I didn't want to support to something I will not support.

## So, what do I do?

I don't have any good advice but I do have general advice. Upskill. Keep learning. Get better. There are plenty of tools out there, even some drag and drop ones that offer blog-like content management systems.

For me, it's [Astro](https://astro.build/), *my second plug*. I like the ability to write in [Markdown](https://www.markdownguide.org/basic-syntax/) and have my website render it. It's not for everyone - but it works for me.

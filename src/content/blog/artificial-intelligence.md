---
title: Artificial Intelligence
description: Letting out some thoughts on A.I. and some of the hype surrounding it.
category: Blog
tags:
  - Artifact
  - Robot
pubDate: 2023-12-10
blueskyUri: 3lfxyv3krb22a
---

I'm not afraid of it. Are we afraid of global warming? Or global instability? It's 2023 and there's a war happening right now that is very televised that some are already "bored" of. And there's a few others that aren't even on the channels.

I believe AI will be our next paradigm, like the television and the toaster. Maybe it'll be our last one. It could be that all the rest, we'll have to credit to the robots. Surely it will affect the world or the universe or somewhere in between. It'll be quick or slow or constrained or not. As its own intelligence it may see us as a threat, or care about us the same way we care about ants. Or maybe it'll help us in our endeavours too. But it will happen eventually; this thing people call the [*singularity*](https://en.wikipedia.org/wiki/Technological_singularity).

Just a note on my beliefs. They're not truths. And truths have no regard for beliefs. It's more like an emotional guess for me. I may not be as, or take in as much information as others, but I can mostly keep a level head when talking about these things. These sorts of topics tend to heat up the room, so I try to steer clear. It is a big topic though that includes voices from heavy hitters like Steven Hawkins and Jeffrey, who changed his last name to Hawkins.

Computers are already intelligent. Check the definition. Wise, no. Because they fail on the "good judgement" part. Good. What is "good"? Then check:

- Desire
- Feelings
- Emotion

If this whole thing was labelled "Artificial Wisdom", would it invoke the same kind of fear? It should. It's like the word "Intelligence" is a marketing campaign same as words like "Piracy" and "Terrorism".

I certainly fear intelligence. Especially in well written characters; Anton, Hans Landa, Mr. Lecter... But Hal just isn't that scary. It's most likely desensitisation. That movie was written some time ago. That movie was made when these kinds of ideas were make believe. It was eerie and gave discomfort and dread for the future of humanity. Wow. Now kids have the same arguments with Alexa, so they can watch nudy pics on the net.

Have the doomsday goalposts just shifted a little? I hope so. We still walk around with signs and preach in the streets but now in freaking casual conversation. It gets tiring. Let's see it happen. It'll probably do a lot of good. Or maybe humans go extinct. But, at least in that scenario, when aliens debug all the mess and sift through the remains; I hope they'll realise at the core - the cause at the heart of it at all was; **blame shifting**.

Aside from random events, which are becoming less and less random with the advancement of technology; we are kind of the dictators of our own demise. We have the same amount of choice as we've ever had, actually probably a lot more. Take that with a grain of salt, though. The more we find out, the more we find out what we don't know.

And that should be positive, inspiring and a learning experience. Like binge-watching episodes of Andor. And it's not like you have no control. You can sit your ass down and learn it. One bite at a time. If you intimately know a bible or something, then this should be a piece of cake. Just drop the ego and bias (like that's an easy thing to do) and I'm sure you'll eventually lose the fear.

I'm gonna finish this short with a couple of quotes, one from a famous little green guy, the other from ... I don't know:

> Fear is the path to the dark side. Fear leads to anger. Anger leads to hate. Hate leads to suffering.

So, please at the least, have a dealing with your fears - and:

> The path to hell is paved with good intentions.

Nothing intended. But, I'd love to meet Albert there and talk about the mushrooms in Caputh.

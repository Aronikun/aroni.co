import { defineCollection, z } from "astro:content";

const blog = defineCollection({
  schema: z.object({
    title: z.string(),
    description: z.string().optional(),
    draft: z.boolean().optional(),
    pubDate: z.coerce.date().optional(),
    upDate: z.coerce.date().optional(),
    order: z.number().optional(),
    image: z
      .object({
        src: z.string().optional().transform((val) => !val?.startsWith("/img/blog-feature/") ? `/img/blog-feature/${val}` : val),
        alt: z.string().optional(),
      })
      .optional(),
    tags: z.array(z.string()).optional(),
    category: z.string().optional(),
    blueskyUri: z.string().optional(),
  }),
});

export const collections = { blog };

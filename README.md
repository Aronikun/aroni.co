# aroni.co

![Bowing Aroni](./assets/favicon.png)

Hello! My name is Aroni and this is my website. It's built using the framework [AstroJS](http://astro.build). I use a few frameworks at work, like [react](https://react.dev/), [vue](https://vuejs.org/), their children and others but I wanted a blog and a static site. So I tried:

- [Jekyll](https://jekyllrb.com/)
- [Hugo](https://gohugo.io/)
- [VitePress](https://vitepress.dev/)
- and [11ty](https://www.11ty.dev/) which deserves great praise.

But I settled with Astro because:

- Perfect Lighthouse scores
- Usage of components out of the box
- Develop in HTML and CSS with superpowers, not quasi pseudo abomination stuff
- Able to use that quasi stuff if you really have to
- Data fetch from any CMS
- Content in [Markdown](https://en.wikipedia.org/wiki/Markdown)
- [discord](https://astro.build/chat) is great

If you're looking into getting into developing using Astro start with the [documentation](https://docs.astro.build/en/getting-started/) or you can get stuck in by [building your first Astro blog](https://docs.astro.build/en/tutorial/0-introduction/).

 If you like being taught from a teacher, [Chris Pennington](https://learnastro.dev/) has a [free blog course on YouTube](https://www.youtube.com/playlist?list=PLoqZcxvpWzzeRwF8TEpXHtO7KYY6cNJeF).

## Why do I want a website?

My main source of income is primarily in web development but in all the years I have been developing, I have never had the need of a personal website. I still don't need one. This exists for me to have fun. I enjoy making websites. So here we are.

When I was young, I loved [ActionScript](https://en.wikipedia.org/wiki/ActionScript). I thought that would be the way of the web. It wasn't. CSS was the closest thing to tickle that fancy. I know there are Javascript libraries out there but I just don't like Javascript so CSS will do.

## Start with a feature

Originally I had a few goals for this website. Now, these are bit more fluid.

- To keep a running portfolio on the things that I work on and take an interest in.
- To offer my services as a web developer or designer.
- To help potential clients understand the development process.
- To keep learning and to try to keep up with technology.

I'm not really after work, so offering services and and potential clients is not priority. The others, yes. But there's no feature there. So the feature I started with is an animation I played in my head. I like UI animation *as you may have picked up*. It was some kind of eye that you can poke. And here it is:

![Eye poking gif](./assets/eye-poke.gif)

And then it cascades from there.

- Animated text. Something unobtrusive. Something simple. Maybe something that you might not even notice is animating. Something that makes you think "Am I high?"
- A portrait that shows my insides. Basically things I get into and maybe some of the work that I do.
- Blog
- Posts sorted into cards
- Interesting animations on those cards
- Style some to be appropriate to their topic - eg. MTG cards for MTG related posts
- Demonstration of animation skills - do something thats over the top and CSS only, no library.

Things that are for later.

- Contact page
- Users can comment on blog posts
- Users can log in
- Useless character creator - Always wanted to make a character creator

## Goals

- 100% [Lighthouse](https://developer.chrome.com/docs/lighthouse/overview) score
- Don't get hung up on perfecting it. Just keep shipping it.
- Try to avoid Javascript where I can. Unfortunately random numbers need this. Generative search too.

## Contact and support

Please support me.

You can do this by leaving a comment on any of my blog posts, sharing them on any social media where you think appropriate or generally just taking an interest.

I'm a beginner and I always will be. If there's something on my site that you'd like to see a tutorial for, send me a comment on my contact page asking and I'll do my best to make a post telling you how I did it.

(*TODO - make contact page*)
